# clj-lanterna

A Clojurey wrapper around the Lanterna terminal output library.

This is a fork of a fork of the [MultiMud/clojure-lanterna](https://github.com/MultiMUD/clojure-lanterna) on github. There was an effort in early 2017 to [Update to Lanterna 3.0](https://github.com/MultiMUD/clojure-lanterna/pull/21), but the code was never merged to the master branch or released to clojars. In late 2017 this fork [AvramRobert/clojure-lanterna](https://github.com/AvramRobert/clojure-lanterna) was released to clojars with the version `0.11.0`. It picks up from where the aboive PR left off, finalizes a few details and merges support for basic `terminal` and `screen` functions with Lanterna 3.0 to `master`. That is where this project's fork begins. This project had a roadmap with an ambition to "fully support Lanterna 3." I don't even know what that means, but I want to try writing TUI applications with Clojure and Lanterna 3 so let's see if I can pick up the torch.

## Leiningen

```clojure
    ;; TODO: Make this available at some point. Right now it's not there.
    [org.clojars.willvaughn/clj-lanterna "0.11.0"]
```

## License

Copyright 2020 William Vaughn

Copyright 2017 Martin S. Weber and contributors

Copyright 2012-2016 Steve Losh and contributors

Distributed under the GNU Lesser General Public License, the same as Lanterna
itself.
