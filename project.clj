(defproject org.clojars.willvaughn/clj-lanterna "0.11.0"
  :description "A Clojure wrapper around the Lanterna terminal output library."
  :url "https://gitlab.com/willvaughn/clj-lanterna"
  :license {:name "LGPL"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [com.googlecode.lanterna/lanterna "3.1.0-beta1"]]
  :java-source-paths ["./java"]
  :repositories {"releases" {:url "https://clojars.org/repo"
                             :username :env
                             :password :env
                             :sign-releases false}})
